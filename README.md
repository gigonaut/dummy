# Dummy

Dummy is an atomic framework for developing new sites.

## Build Environment
Dummy now uses gulp and browserify to put itself together.

### The Basics
You need node and npm installed obviously. You will also need gulp installed globally.

```git clone the repo```

```npm install```

```gulp //will build the project```

The project will be build and the final buildfiles will be deposited in the public directory and can be served from there.

You can also

```gulp watch```

and that will allow you to make changes and have them build on on the fly.

## CSS Structure

This outlines the logic behind the structure of the `.scss` files.

### Comments

The scss files are commented to help promote consistency and organization. Listed below are a few of the most common elements and where they should be used.

**Headings/Dividers**
Use this heading format to separate sections within scss files.
```css
/* -------------------------------------------------------------- Heading */
```

**Subheadings**
Use this format to separate out smaller divisions within heading sections.
```css
/* Subhead */
```

**Line Comments**
Use standard one line comments to annotate sections of css.
```css
div {
	// Annotated css.
	width: auto;
	height: auto;
}
```
- - -

### Reset

The reset stylesheet contains all basic resets for the project. Any additional resets for the whole project should be added here.

- - -

### Mixins

Contains a listing of all the most common mixins categorized by type. Any additional mixins should be added under the MISC heading.

- - -

### Variables

Variables are defined in the `main.scss` file and should be separated by type with Subheadings.

- - -

### Navigation

Any site navigation elements should be defined in `_navigation.scss`. The starting elements are `.menu` and `.breadcrumbs`. You can add or remove elements as needed.

- - -

### Page

The `_page.scss` file contains high-level page styles (for example - any styles that would need to be specific to a `.content` area or a `.sidebar` area), and also page-specific styles. A good example of something that might fall under page-specific styles would be formatted contact info specific to the contact page.

- - -

### Footer

This could also just be moved to `_page.scss`, but if you have a lot of custom styles for the footer they should be kept in a separate file.

- - -

### Forms

All form styles live here. If you need to create several differently styled forms (like a contact form and a search form) they should be separated by headings and labeled appropriately.

- - -

### Elements

See the elements section for a detailed description of how the elements should be organized.

- - -

### Responsive

All responsive files live here. Each break should be styled as such:

```css
/* -------------------------------------------------------------- (Size Label) */

@media (max-width: //Breakpoint in pixels) {
/* Start (Size Label) Styles */

// Styles go here

/* End (Size Label) Styles */
}
```

Based on the way most designs are produced I've opted for a "desktop down" model - meaning that the styles defined in the main stylesheets describe the Desktop version of the project and the responsive stylesheet goes from largest screen to smallest screen.

Feel free to go with a mobile-first process and alter the responsive sheet accordingly, but be sure to keep the project in mind.

- - -
- - -

## Elements

This outlines the logic and basic use instructions for the `_elements_` scss files.

### Text

Defines selected text styles, placeholder text for forms, paragraph styles, global list styles, and heading styles. If there are other text elements that need global styles they should be placed here as well.

- - -

### Interactive

The default version contains predefined styles or class names for: notificatons, buttons, dropdown lists, responsive video embeds, and a modal overlay. Any other elements that the user would interact with that are not navigation should be added here. A good example would be tooltips.

- - -

### Modules

This file contains all self-contained elements. Any modular item that will be reused throughout the site should be here.

- - -

### Callouts

Callouts can include anything from featured text blocks to interactive links to other pages or content. More specific than modules, these items should mainly be used for link areas or major attention areas.

- - -
- - -

## HTML Structure

All these elements can be found in `elements.html`, but they are listed here for quick reference.

### Header

An example of a basic header

```html
<!DOCTYPE html> <!-- Use HTML5 doctype specification. -->
<html>
<head>

<meta charset="utf-8"> <!-- Define UTF-8 charset. -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> <!-- Set to latest version of IE. -->
<meta name="viewport" content="width=1224"> <!-- Define viewport width. Should be set to width=device-width,initial-scale=1.0 for responsive designs. -->

<link rel="shortcut icon" href="" /> <!-- Favicon. -->

<link rel="stylesheet" href="css/main.css"> <!-- SCSS output stylesheet. -->

<!-- Paste any code for webfonts here. -->

<!--[if lt IE 9]>
<script src="js/vendor/html5shiv.js"></script>
<![endif]-->

<!-- HTML5shiv for older versions of IE. -->

<title>Dummy Elements</title> <!-- Page title. -->
<meta name="description" content=""> <!-- Page description. -->

</head>
```

- - -

### Menu

Basic HTML for a menu element, including the optional dropdown menu.

```html
<!-- Menu -->

<header class="menu">

	<nav class="nav-primary">
		<a href="#">One Item</a>

		<!-- Dropdown Menu -->
		<div class="nav-secondary">
			<span>Two Item</span>

			<nav class="nav-dd">
				<a href="#">One Subitem</a>
				<a href="#">Two Subitem</a>
				<a href="#">Three Subitem</a>
				<a href="#">Four Subitem</a>
			</nav>
		</div>
		<!-- Dropdown Menu -->

		<a href="#">Three Item</a>

		<a href="#">Four Item</a>

		<a href="#">Five Item</a>
	</nav>

</header>
```

## Scripts

For non-jquery projects, Dummy uses [Ender.js](http://enderjs.com/) to handle JS libraries.

Install with `npm install ender -g`

Then set up with `ender build package-list`

A few good libraries to work with:

### Domready

Dom.... ready? [github.com/ded/domready](http://github.com/ded/domready).

### Qwery

Selector engine [github.com/ded/qwery](http://github.com/ded/qwery).

### Bonzo

DOM utility (.addClass, .removeClass, etc) [github.com/ded/bonzo](http://github.com/ded/bonzo).

### Bean

Events API (.on, .off, .clone, .fire, etc) [github.com/fat/bean](https://github.com/fat/bean).

### Velociy.js

Animations [github.com/julianshapiro/velocity](http://github.com/julianshapiro/velocity).
