var gulp         = require('gulp'),
    browserify   = require('browserify'),
    transform    = require('vinyl-transform'),
    uglify       = require('gulp-uglify'),
    sequence     = require('run-sequence').use(gulp),
    rm           = require('gulp-rimraf'),
    vendor       = require('gulp-concat-vendor'),
    sass         = require('gulp-sass'),
    concat       = require('gulp-concat'),
    isProduction = false,
    fs           = require('fs');

var globs = {
    sass:   './client/css/**/*',
    js:     'client/js/**/*',
    html:   'client/**/*.html',
    images: 'client/**/*.{jpg,jpeg,svg,png,gif}',
    misc:   'client/**/*.{ico,eot,woff,ttf,xml,swf,mp3,wav,aif}'
};

var publishDir = './public';

gulp.task('clean', function() {
  return gulp.src(publishDir + '/*').pipe(rm());
});

gulp.task('js', function () {
    var browserified = transform(function(filename) {
        var b = browserify(filename);
        return b.bundle();
    });
      // hello gulp.src() my old friend
    return gulp.src(globs.js)
    .pipe(browserified)
    .pipe(uglify())
    .pipe(gulp.dest(publishDir + '/js'));
});

gulp.task('sass',  function () {
  return gulp.src(globs.sass)
  .pipe(sass())
  .pipe(gulp.dest(publishDir+ '/css/'));
});

gulp.task('html', function() {
  return gulp.src(globs.html)
  .pipe(gulp.dest(publishDir))
});

gulp.task('watch', function() {
    gulp.watch(globs.vendor, ['vendor']);
    gulp.watch(globs.html, ['html']);
    gulp.watch(globs.js, ['js']);
    gulp.watch(globs.wsass, ['sass']);
});

gulp.task('build', function() {
    sequence('clean', ['sass', 'html', 'js']);
});

gulp.task('default', ['build']);