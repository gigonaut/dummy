var $ = require('jquery');
var riot = require('riotjs');

$(document).ready(function() {

	// Dropdown
	$('.dropdown').click(function(e) {
		$(this).children('.dd-list').slideToggle(250);
		e.preventDefault();
	});

	// Select
	$('.select').click(function(e) {
		$(this).children('.select-dd').slideToggle(250);
		e.preventDefault();
	});

	// Modal
	$('.modal-btn').click(function(e) {
		var modalId = $(this).attr('data-modal-id');

		$('body').toggleClass('modal-body');
		$('div#' + modalId).fadeToggle(250);
		e.preventDefault();
	});

	$('.modal-overlay').click(function(e) {
    if (e.target == this) {
    	$('body').toggleClass('modal-body');
			$('.modal-overlay').fadeToggle(250);
    }
  });

});